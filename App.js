import React from 'react';
import {ScrollView, View, Text, StyleSheet} from 'react-native';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {NavigationContainer} from '@react-navigation/native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import TransferenceStack from './navigators/TransferenceStack';

const Tab = createBottomTabNavigator();

const ExampleComponent = () => <Text> The component name is </Text>;

const App = () => {
  return (
    <>
      <NavigationContainer>
        <Tab.Navigator
          screenOptions={({route}) => ({
            tabBarIcon: ({focused, color, size}) => {
              let iconName;
              switch (route.name) {
                case 'Home':
                  iconName = focused ? 'home' : 'home-outline';
                  break;
                case 'Transference':
                  iconName = focused
                    ? 'account-arrow-right'
                    : 'account-arrow-right-outline';
                  break;
                case 'Detail':
                  iconName = focused ? 'ballot' : 'ballot-outline';
                  break;
              }
              return <Icon color={color} size={size} name={iconName} />;
            },
          })}>
          <Tab.Screen name="Home" component={ExampleComponent} />
          <Tab.Screen name="Transference" component={TransferenceStack} />
          <Tab.Screen name="Detail" component={ExampleComponent} />
        </Tab.Navigator>
      </NavigationContainer>
    </>
  );
};

export default App;
