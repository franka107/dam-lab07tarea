import React from 'react';
import {View, Text} from 'react-native';
import {createStackNavigator} from '@react-navigation/stack';
import FirstTransferenceScreen from '../screens/FirstTransferenceScreen';
import SecondTransferenceScreen from '../screens/SecondTransferenceScreen';
import ThirdTransferenceScreen from '../screens/ThirdTransferenceScreen';

const Stack = createStackNavigator();

const TransferenceStack = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="First"
        options={{
          headerStyle: {
            backgroundColor: 'skyblue',
          },
        }}
        component={FirstTransferenceScreen}
      />
      <Stack.Screen
        name="Second"
        options={{
          headerStyle: {
            backgroundColor: 'aquamarine',
          },
        }}
        component={SecondTransferenceScreen}
      />

      <Stack.Screen
        name="Third"
        options={{
          headerStyle: {
            backgroundColor: 'greenyellow',
          },
        }}
        component={ThirdTransferenceScreen}
      />
    </Stack.Navigator>
  );
};

export default TransferenceStack;
