import React from 'react';
import {View, StyleSheet} from 'react-native';
import {
  HelperText,
  TextInput,
  Text,
  Title,
  Button,
  Switch,
} from 'react-native-paper';

const SecondTransferenceScreen = ({route, navigation}) => {
  const {originAccount} = route.params;
  const {endAccount} = route.params;
  const {myimport} = route.params;
  const {reference} = route.params;
  const {dateTransference} = route.params;
  const {isNotifyEmail} = route.params;
  return (
    <View style={styles.container}>
      <Title style={styles.title}>Inicia tu transferencia</Title>
      <TextInput
        label="Cuenta de origen"
        value={originAccount}
        mode="outlined"
        disabled={true}
      />
      <TextInput
        label="Cuenta de destino"
        value={endAccount}
        mode="outlined"
        disabled={true}
      />
      <TextInput
        label="Importe"
        value={myimport}
        mode="outlined"
        disabled={true}
      />
      <TextInput
        label="Referencia"
        value={reference}
        mode="outlined"
        disabled={true}
      />
      <TextInput
        label="Fecha"
        value={dateTransference}
        mode="outlined"
        disabled={true}
      />
      <TextInput
        label="Mail"
        value={isNotifyEmail ? 'SI' : 'NO'}
        mode="outlined"
        disabled={true}
      />
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-around',
          marginTop: 10,
        }}>
        <Button mode="contained" onPress={() => navigation.goBack()}>
          Atras
        </Button>
        <Button
          mode="contained"
          color="green"
          onPress={() => navigation.navigate('Third')}>
          Confirmar
        </Button>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    padding: 20,
  },
  title: {textAlign: 'center'},
  emailForm: {
    flexDirection: 'row',
    justifyContent: 'center',
    padding: 20,
  },
  emailLabel: {
    textAlignVertical: 'center',
    fontSize: 15,
  },
});
export default SecondTransferenceScreen;
