import React from 'react';
import {View, Text} from 'react-native';

const ThirdTransferenceScreen = () => {
  return (
    <View style={{flex: 1, flexDirection: 'column', justifyContent: 'center'}}>
      <Text
        style={{
          fontSize: 30,
          textAlign: 'center',
          textAlignVertical: 'center',
          color: 'green',
          fontWeight: 'bold',
        }}>
        La transferencia bancaria a sido un exito
      </Text>
    </View>
  );
};

export default ThirdTransferenceScreen;
