import React, {useState} from 'react';
import {View, StyleSheet, TouchableOpacity} from 'react-native';
import {
  HelperText,
  TextInput,
  Text,
  Title,
  Button,
  Switch,
} from 'react-native-paper';
import DateTimePickerModal from 'react-native-modal-datetime-picker';
import moment from 'moment';
moment.locale('es');

const FirstTransferenceScreen = ({navigation}) => {
  const [originAccount, setOriginAccount] = useState('');
  const [endAccount, setEndAccount] = useState('');
  const [myimport, setMyImport] = useState('');
  const [reference, setReference] = useState('');
  const [isNotifyEmail, setIsNotifyEmail] = useState(false);
  const [dateTransference, setDateTransference] = useState(
    moment().format('DD/MM/YYYY'),
  );
  const [dateTransferenceFormatted, setDateTransferenceFormatted] = useState(
    moment(dateTransference).toDate(),
  );
  const [isDatePickerVisible, setDatePickerVisibility] = useState(false);

  const onlyNumbersVerification = (account) => {
    let reg = /^\d+$/;
    return account ? !reg.test(account) : false;
  };
  const handleConfirm = (date) => {
    setDatePickerVisibility(false);
    setDateTransference(moment(date).format('DD/MM/YYYY'));
    setDateTransferenceFormatted(moment(date).toDate());
  };
  return (
    <View style={styles.container}>
      <Title style={styles.title}>Inicia tu transferencia</Title>
      <TextInput
        label="Cuenta de origen"
        value={originAccount}
        onChangeText={(text) => setOriginAccount(text)}
        error={onlyNumbersVerification(originAccount)}
        mode="outlined"
      />
      <HelperText type="error" visible={onlyNumbersVerification(originAccount)}>
        Solo números
      </HelperText>
      <TextInput
        label="Cuenta de destino"
        value={endAccount}
        onChangeText={(text) => setEndAccount(text)}
        error={onlyNumbersVerification(endAccount)}
        mode="outlined"
      />
      <HelperText type="error" visible={onlyNumbersVerification(endAccount)}>
        Solo números
      </HelperText>
      <TextInput
        label="Importe"
        value={myimport}
        onChangeText={(text) => setMyImport(text)}
        error={onlyNumbersVerification(myimport)}
        mode="outlined"
      />
      <HelperText type="error" visible={onlyNumbersVerification(myimport)}>
        Solo se permiten números
      </HelperText>
      <TextInput
        label="Referencia"
        value={reference}
        onChangeText={(text) => setReference(text)}
        mode="outlined"
        multiline={true}
      />
      <HelperText type="error" visible={false}>
        Solo se permiten números
      </HelperText>
      <TouchableOpacity
        onPress={() => {
          setDatePickerVisibility(true);
        }}>
        <TextInput
          label="Fecha"
          value={dateTransference}
          mode="outlined"
          editable={false}
        />
      </TouchableOpacity>
      <DateTimePickerModal
        isVisible={isDatePickerVisible}
        mode="date"
        onCancel={() => setDatePickerVisibility(false)}
        onConfirm={handleConfirm}
      />
      <HelperText type="error" visible={false}>
        Solo se permiten números
      </HelperText>
      <View style={styles.emailForm}>
        <Text style={styles.emailLabel}>Notificarme al email</Text>
        <Switch
          value={isNotifyEmail}
          onValueChange={() => {
            setIsNotifyEmail(!isNotifyEmail);
          }}
        />
      </View>
      <Button
        mode="contained"
        onPress={() =>
          navigation.navigate('Second', {
            originAccount,
            endAccount,
            myimport,
            reference,
            dateTransference,
            isNotifyEmail,
          })
        }>
        Enviar
      </Button>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    padding: 20,
  },
  title: {textAlign: 'center'},
  emailForm: {
    flexDirection: 'row',
    justifyContent: 'center',
    padding: 20,
  },
  emailLabel: {
    textAlignVertical: 'center',
    fontSize: 15,
  },
});

export default FirstTransferenceScreen;
